<?php

use Illuminate\Support\Facades\Route;

Route::prefix('dashboard')
    ->name('dashboard.')
//    ->middleware(['auth'])
//    ->middleware(['auth','role:super_admin,admin'])
    ->group(function (){

    //dashboard.welcome  - welcome_Route
        Route::get('/','welcomeController@index')->name('welcome');

    //  category Routes
        Route::resource('categories','CategoryController')->except(['show']);

    //  contact_us
            Route::resource('contact_us','ContactUsController')->except(['show']);

    //  Role Route
        Route::resource('roles','RoleController');

    //  User Route
        Route::resource('users','UserController');

    //  User Route
        Route::resource('benefactors','BenefactorController');

    //  User Route
        Route::resource('classifications','ClassificationController');

    //  User Route
        Route::resource('needies','NeedyController');

    // Setting Route
        Route::get('/settings/social_login','SettingController@Social_Login')->name('settings.social_login');
        Route::get('/settings/social_link','SettingController@Social_Links')->name('settings.social_links');
        Route::post('/social_links','SettingController@store')->name('settings.store');



//  Donation Route
//     Route::resource('donations','DonationController');


        //
//     //Patient
//        Route::resouarce('patients','PatientController');
//
//     //classifications
//        Route::resource('classifications','PatientsClassificationController');
//
//     //Describe Treatment
//        Route::resource('describe_treatments','DescribeTreatmentController');

        //
//    //sub Categories
//        Route::resource('sub_categories','SubCategoryController')->except(['show']);
//
//    //consultation_requests
//        Route::resource('consultation_requests','Consultation_requestsController')->except(['show']);
//
//     //serviceItem
//        Route::resource('serviceItem','ServiceItemController')->except(['show']);
//
//     //advertisement
//        Route::resource('advertisement','AdvertisementController')->except(['show']);
//
//     //advertisementItem
//        Route::resource('advertisementItems','AdvertisementItemsController')->except(['show']);
//
//    //WhoAreWe
//            Route::resource('WhoAreWes','whoAreWeController')->except(['show']);
//


    });
