@extends('layouts.dashboard.app')

@section('content')
    <div class="app-title">
        <div>
            @if(isset($classification)  )
                <h1>
                    <i class="fa fa-edit">
                        {{ __('site.Update Patients Classification')}}
                    </i>
               </h1>
            @else
                <h1>
                    <i class="fa fa-plus">
                      {{__('site.Add Patients Classification') }}
                    </i>
                </h1>
            @endif

        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-list"></i></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">{{__('site.Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.classifications.index')}}">{{__('site.Patients Classification')}}</a></li>
            @if(isset($classification))
                <li class="breadcrumb-item">{{__('site.Update Patients Classification')}}</li>
            @else
                <li class="breadcrumb-item">{{__('site.Add Patients Classification')}}</li>
            @endif
        </ul>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-md-12">
             <form action="{{isset($classification)?route('dashboard.classifications.update',$classification->id):route('dashboard.classifications.store')}}" method="post">
                 @csrf
                 @if(isset($classification))
                     @method('put')
                 @else
                     @method('post')
                 @endif

                 @include('dashboard.partials._errors')

                 @if(isset($classification))
                     {{-- Select of Patient --}}
                      <div class="form-group">
                          <label>{{__('site.Patient Name')}} :</label>
                          <select name="patient_id" class="form-control">
                             @foreach( $patients as $patient )

                                  <option value="{{$patient->id}}" {{$patient->id == $classification->patient_id?"SELECTED":""}}> {{ $patient->name }}</option>
                             @endforeach
                          </select>
                      </div>
                 @else
                     {{-- Select of Patient --}}
                     <div class="form-group">
                         <label>{{__('site.Patient Name')}} :</label>
                         <select name="patient_id" class="form-control">
                             @foreach( $patients as $patient )
                                 <option value="{{$patient->id}} "> {{ $patient->name }}</option>
                             @endforeach
                         </select>
                     </div>

                 @endif
                 @if(isset($classification))
                 {{-- Select of Severity degree --}}
                 <div class="form-group">
                     <label>{{__('site.Severity degree')}} :</label>
                     <select name="severity_degree" class="form-control">
                         <option value="1" {{  $classification->severity_degree == "low" ? "SELECTED":"" }}> {{__('site.low') }}</option>
                         <option value="2" {{  $classification->severity_degree == "middle" ? "SELECTED":"" }}> {{__('site.middle') }}</option>
                         <option value="3" {{  $classification->severity_degree == "high" ? "SELECTED":"" }}> {{__('site.high') }}</option>
                     </select>
                 </div>
                 @else
                  {{-- Select of Severity degree --}}
                 <div class="form-group">
                     <label>{{__('site.Severity degree')}} :</label>
                     <select name="severity_degree" class="form-control">
                         <option value="1"> {{__('site.low') }}</option>
                         <option value="2" }> {{__('site.middle') }}</option>
                         <option value="3"> {{__('site.high') }}</option>
                     </select>
                 </div>

                 @endif
                 <div class="form-group">
                     <label>{{__('site.Notes')}} :</label>
                     <textarea name="notes" cols="30" rows="10"  class="form-control">{{isset($classification)?$classification->notes:""}}</textarea>
                 </div>

                 <div class="form-group">
                     <button type="submit" class="btn btn-primary">
                         @if( isset($classification) )
                             <i class="fa fa-edit"></i>
                             {{__('site.Update')}}
                         @else
                             <i class="fa fa-plus"></i>
                             {{__('site.Add')}}
                         @endif

                     </button>
                 </div>
             </form>

            </div>{{-- end-of-col-12 --}}
        </div>{{--end-of-row--}}


    </div>{{--end-of-tile mb-4--}}


@endsection
