@extends('layouts.dashboard.app')

@section('content')
{{--    header  --}}
<div class="app-title">
    <div>
        <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
    </div>
    <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
    </ul>
</div>




{{--<div class="card-deck">--}}
{{--    card 1    --}}
{{--    <div class="card text-white bg-primary mb-3">--}}
{{--        <div class="card-header">Card 1 : Consultation Requests</div>--}}
{{--        <div class="card-body">--}}
{{--            <h5 class="card-title"> Consultation Requests Number</h5>--}}
{{--            <h1 class="card-text">#NO : {{$Consultation_requests->count()}}   </h1>--}}
{{--            {{$last_Consultation_requests->name}}--}}
{{--        </div>--}}
{{--        <div class="card-footer">--}}
{{--            <small class="card-text">Last updated <b><h6>{{ $last_Consultation_requests->created_at->diffForHumans()   }} </h6></b> </small>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--    --}}{{--    card 2    --}}
{{--    <div class="card text-white bg-success mb-3">--}}
{{--        <div class="card-header">Card 2 : Services</div>--}}
{{--        <div class="card-body">--}}
{{--            <h5 class="card-title">Our Services Number</h5>--}}
{{--            <h1 class="card-text">#NO : {{$item_service->count()}}</h1>--}}
{{--        </div>--}}
{{--        <div class="card-footer">--}}
{{--            <small class="card-text">Last updated <b><h6>{{ $last_item_service->created_at->diffForHumans()  }} o</h6></b> </small>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--    --}}{{--    card 3    --}}

{{--    <div class="card text-white bg-dark mb-3">--}}
{{--        <div class="card-header">Card 3 : Advertisments</div>--}}
{{--        <div class="card-body">--}}
{{--            <h5 class="card-title">Our Advertisments Number</h5>--}}
{{--            <h1 class="card-text">#NO : {{$AdvertisementItems->count()}}</h1>--}}
{{--        </div>--}}
{{--        <div class="card-footer">--}}
{{--            <small class="card-text">Last updated <b><h6>{{ $last_AdvertisementItems->created_at->diffForHumans()  }} </h6></b> </small>--}}
{{--        </div>--}}
{{--    </div>--}}


{{--</div>--}}

@endsection
