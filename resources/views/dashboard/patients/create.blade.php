@extends('layouts.dashboard.app')

@section('content')
    <div class="app-title">
        <div>
            @if(isset($patient)  )
                <h1>
                    <i class="fa fa-edit">
                        {{ __('site.Update Patient')}}
                    </i>
               </h1>
            @else
                <h1>
                    <i class="fa fa-plus">
                      {{__('site.Add Patient') }}
                    </i>
                </h1>
            @endif

        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-list"></i></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">{{__('site.Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.patients.index')}}">{{__('site.patients')}}</a></li>
            @if(isset($patient))
                <li class="breadcrumb-item">{{__('site.Update Patient')}}</li>
            @else
                <li class="breadcrumb-item">{{__('site.Add Patient')}}</li>
            @endif
        </ul>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-md-12">
             <form action="{{isset($patient)?route('dashboard.patients.update',$patient->id):route('dashboard.patients.store')}}" method="post">
                 @csrf
                 @if(isset($patient))
                     @method('put')
                 @else
                     @method('post')
                 @endif

                 @include('dashboard.partials._errors')

                 <div class="form-group">
                     <label>{{__('site.name')}} :</label>
{{--                     <textarea name="name" cols="30" rows="10"  class="form-control">{{isset($patient)?$patient->name:""}}</textarea>--}}
                     <input type="text" name="name" class="form-control" value="{{isset($patient)?$patient->name:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.email')}} :</label>
{{--                     <textarea name="email" cols="30" rows="10"  class="form-control">{{isset($patient)?$patient->email:""}}</textarea>--}}
                     <input type="text" name="email" class="form-control" value="{{isset($patient)?$patient->email:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.phone1')}} :</label>
{{--                     <textarea name="phone1" cols="30" rows="10"  class="form-control">{{isset($patient)?$patient->phone1:""}}</textarea>--}}
                     <input type="text" name="phone1" class="form-control" value="{{isset($patient)?$patient->phone1:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.phone2')}} :</label>
{{--                     <textarea name="phone2" cols="30" rows="10"  class="form-control">{{isset($patient)?$patient->phone2:""}}</textarea>--}}
                     <input type="text" name="phone2" class="form-control" value="{{isset($patient)?$patient->phone2:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.sugar_percentage')}} :</label>
{{--                     <textarea name="sugar_percentage" cols="30" rows="10"  class="form-control">{{isset($patient)?$patient->sugar_percentage:""}}</textarea>--}}
                     <input type="text" name="sugar_percentage" class="form-control" value="{{isset($patient)?$patient->sugar_percentage:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.blood_pressure')}} :</label>
{{--                     <textarea name="blood_pressure" cols="30" rows="10"  class="form-control">{{isset($patient)?$patient->blood_pressure:""}}</textarea>--}}
                     <input type="text" name="blood_pressure" class="form-control" value="{{isset($patient)?$patient->blood_pressure:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.washing_time_week')}} :</label>
{{--                     <textarea name="washing_time_week" cols="30" rows="10"  class="form-control">{{isset($patient)?$patient->washing_time_week:""}}</textarea>--}}
                     <input type="number" name="washing_time_week" class="form-control" value="{{isset($patient)?$patient->washing_time_week:""}}">
                 </div>

                 <div class="form-group">
                     <button type="submit" class="btn btn-primary">
                         @if( isset($patient) )
                             <i class="fa fa-edit"></i>
                             {{__('site.Update')}}
                         @else
                             <i class="fa fa-plus"></i>
                             {{__('site.Add')}}
                         @endif

                     </button>
                 </div>
             </form>

            </div>{{-- end-of-col-12 --}}
        </div>{{--end-of-row--}}


    </div>{{--end-of-tile mb-4--}}


@endsection
