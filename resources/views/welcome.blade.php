@include('header')

<!-- ***** Header Area Start ***** -->
<header class="header-area header-sticky">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="main-nav">
                    <!-- ***** Logo Start ***** -->
                    <a href="#" class="logo">
                        <img width="100px" height="100px" style="border: black solid 1px" src="{{asset("assets/images/logo.jpeg")}}" alt="Softy Pinko"/>
                    </a>
                    <!-- ***** Logo End ***** -->
                    <!-- ***** Menu Start ***** -->
                    <ul class="nav">
                        <li><a href="#contact-us">تواصل معنا</a></li>
                        <li><a href="#features">عن الموفع</a></li>
                        <li><a href="#pricing-plans">حالات غسيل الكلى</a></li>
                        <li><a href="#work-process">خدمات الموقع</a></li>
                        <li><a href="#welcome" class="active">الرئيسية</a></li>
                    </ul>
                    <a class='menu-trigger'>
                        <span>االقائمة</span>
                    </a>
                    <!-- ***** Menu End ***** -->
                </nav>
            </div>
        </div>
    </div>
</header>
<!-- ***** Header Area End ***** -->

<!-- ***** Welcome Area Start ***** -->
<div class="welcome-area" id="welcome">

    <!-- ***** Header Text Start ***** -->
    <div class="header-text">
        <div class="container">
            <div class="row">
                <div class="offset-xl-3 col-xl-6 offset-lg-2 col-lg-8 col-md-12 col-sm-12">
                    <h1>نحن لدينا <strong>أفضل</strong><br>جلسات غسيل الكلى  <strong>بواسطة أفضل الأجهزة</strong></h1>
                    <p>كما نصف لك أفضل الأدواية بواسطة أفضل الأطباء</p>
                    <a href="#features" class="main-button-slider">المزيد من النصائح</a>
                </div>
            </div>
        </div>
    </div>
    <!-- ***** Header Text End ***** -->
</div>
<!-- ***** Welcome Area End ***** -->

<!-- ***** Features Small Start ***** -->
<section class="section home-feature">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <!-- ***** Features Small Item Start ***** -->
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12" data-scroll-reveal="enter bottom move 50px over 0.6s after 0.2s">
                        <div class="features-small-item">
                            <div class="icon">
                                <i><img src="{{asset("assets/images/featured-item-01.png")}}" alt=""></i>
                            </div>
                            <h5 class="features-title">أفضل الأطباء</h5>
                            <p>يقوم فريق أطباؤنا بتقديم أفضل العلاجات المناسبة لحالتك الصحية </p>
                        </div>
                    </div>
                    <!-- ***** Features Small Item End ***** -->

                    <!-- ***** Features Small Item Start ***** -->
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12" data-scroll-reveal="enter bottom move 50px over 0.6s after 0.4s">
                        <div class="features-small-item">
                            <div class="icon">
                                <i><img src="{{asset("assets/images/featured-item-01.png")}}" alt=""></i>
                            </div>
                            <h5 class="features-title">نمتلك أفضل أجهزة غسيل الكلى</h5>
                            <p>كفاءة عالية بالاضافة الى جودة و دقة في الأجهزة الطبية فنحن نهتم براحتكم</p>
                        </div>
                    </div>
                    <!-- ***** Features Small Item End ***** -->

                    <!-- ***** Features Small Item Start ***** -->
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12" data-scroll-reveal="enter bottom move 50px over 0.6s after 0.6s">
                        <div class="features-small-item">
                            <div class="icon">
                                <i><img src="{{asset("assets/images/featured-item-01.png")}}" alt=""></i>
                            </div>
                            <h5 class="features-title">ممرضين أكفاء وذو خبرة</h5>
                            <p>لدينا ممرضين يعملون على مدار الساعة طوال أيام الأسبوع 24/7</p>
                        </div>
                    </div>
                    <!-- ***** Features Small Item End ***** -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ***** Features Small End ***** -->

<!-- ***** Features Big Item Start ***** -->
<section class="section padding-top-70 padding-bottom-0" id="features">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-12 col-sm-12 align-self-center" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                <img src="{{asset("assets/images/left-image.png")}}" class="rounded img-fluid d-block mx-auto" alt="App">
            </div>
            <div class="col-lg-1"></div>
            <div class="col-lg-6 col-md-12 col-sm-12 align-self-center mobile-top-fix">
                <div class="left-heading">
                    <h2 class="section-title">دعنا نناقش أمورك الصحية معاً</h2>
                </div>
                <div class="left-text">
                    <p>غسيل الكلى هو إجراء طبي علاجي يتم إجراؤه عادة للتخلص من فضلات الجسم والسوائل الزائدة من الدم عندما تفقد الكلى قدرتها على العمل بشكل صحيح.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="hr"></div>
            </div>
        </div>
    </div>
</section>
<!-- ***** Features Big Item End ***** -->

<!-- ***** Features Big Item Start ***** -->
<section class="section padding-bottom-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12 align-self-center mobile-bottom-fix">
                <div class="left-heading">
                    <h2 class="section-title">نحن نساعدك لتحسن من حالتك الصحية
{{--                        We can help you to improve your Case--}}
                    </h2>


                </div>
                <div class="left-text">
                    <p>

                        يحتاج المريض إلى البروتين للحفاظ على صحة وقوة جسمه ، ويجب الانتباه إلى كمية البروتين المستهلكة ويتم احتساب الكمية المناسبة من البروتين قبل وبعد جلسة غسيل الكلى باستخدام صيغة معينة.
                        ** مصادر البروتين: الدجاج والبيض واللحوم والأسماك.
                    </p>
                </div>
            </div>
            <div class="col-lg-1"></div>
            <div class="col-lg-5 col-md-12 col-sm-12 align-self-center mobile-bottom-fix-big" data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                <img src="{{asset("assets/images/right-image.png")}}" class="rounded img-fluid d-block mx-auto" alt="App">
            </div>
        </div>
    </div>
</section>
<!-- ***** Features Big Item End ***** -->

<!-- ***** Home Parallax Start ***** -->
<section class="mini" id="work-process">
    <div class="mini-content">
        <div class="container">
            <div class="row">
                <div class="offset-lg-3 col-lg-6">
                    <div class="info">
                        <h1>خدمات الموقع</h1>
                        <p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.
                            إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع</p>


                    </div>
                </div>
            </div>

            <!-- ***** Mini Box Start ***** -->
            <div class="row">
                <div class="col-lg-2 col-md-3 col-sm-6 col-6">
                    <a href="#" class="mini-box">
                        <i><img src="{{asset("assets/images/work-process-item-01.png")}}" alt=""></i>
                        <strong>Test Test</strong>
                        <span>Test Test Test Test Test Test.</span>
                    </a>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6 col-6">
                    <a href="#" class="mini-box">
                        <i><img src="{{asset("assets/images/work-process-item-01.png")}}" alt=""></i>
                        <strong>Test Test</strong>
                        <span>Test Test Test Test Test Test.</span>
                    </a>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6 col-6">
                    <a href="#" class="mini-box">
                        <i><img src="{{asset("assets/images/work-process-item-01.png")}}" alt=""></i>
                        <strong>Test Test</strong>
                        <span>Test Test Test Test Test Test.</span>
                    </a>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6 col-6">
                    <a href="#" class="mini-box">
                        <i><img src="{{asset("assets/images/work-process-item-01.png")}}" alt=""></i>
                        <strong>Test Test</strong>
                        <span>Test Test Test Test Test Test.</span>
                    </a>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6 col-6">
                    <a href="#" class="mini-box">
                        <i><img src="{{asset("assets/images/work-process-item-01.png")}}" alt=""></i>
                        <strong>Test Test</strong>
                        <span>Test Test Test Test Test Test.</span>
                    </a>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6 col-6">
                    <a href="#" class="mini-box">
                        <i><img src="{{asset("assets/images/work-process-item-01.png")}}" alt=""></i>
                        <strong>Test Test</strong>
                        <span>Test Test Test Test Test Test.</span>
                    </a>
                </div>
            </div>
            <!-- ***** Mini Box End ***** -->
        </div>
    </div>
</section>
<!-- ***** Home Parallax End ***** -->

<!-- ***** Pricing Plans Start ***** -->
<section class="section colored" id="pricing-plans">
    <div class="container">
        <!-- ***** Section Title Start ***** -->
        <div class="row">
            <div class="col-lg-12">
                <div class="center-heading">
                    <h2 class="section-title">حالات غسيل الكلى</h2>
                </div>
            </div>
            <div class="offset-lg-3 col-lg-6">
                <div class="center-text">
                    <p>يختلف كل مريض عن الآخر في حاجته للغسيل الكلوي ، ودرجة الحاجة هي عامل مهم في قبول طلب الحجز بشكل أسرع.</p>
                </div>
            </div>
        </div>
        <!-- ***** Section Title End ***** -->

        <div class="row">
            <!-- ***** Pricing Item Start ***** -->
            <div class="col-lg-4 col-md-6 col-sm-12" data-scroll-reveal="enter bottom move 50px over 0.6s after 0.2s">
                <div class="pricing-item">
                    <div class="pricing-header">
{{--                        <h3 class="pricing-title"></h3>--}}
                    </div>
                    <div class="pricing-body">
                        <div class="price-wrapper">
                            <span class="price">الحالات البسيطة</span>
{{--                            <span class="period">monthly</span>--}}
                        </div>
                        <ul class="list">
                            <li class="active">2 مرات غسيل في الأسبوع</li>
                            <li class="active">يجب عليه أخذ تمارين رياضية بشكل منتظمة</li>
                            <li class="active">يجب عليه تناول غذاء صحي</li>
                            <li>شرب الكحول</li>
                            <li>شرب السجائر</li>
                            <li>أكل الموالح</li>
                        </ul>
                    </div>
                    <div class="pricing-footer">
                        <a href="{{route("booking")}}" class="main-button">احجز الأن </a>
                    </div>
                </div>
            </div>
            <!-- ***** Pricing Item End ***** -->

            <!-- ***** Pricing Item Start ***** -->
            <div class="col-lg-4 col-md-6 col-sm-12" data-scroll-reveal="enter bottom move 50px over 0.6s after 0.4s">
                <div class="pricing-item active">
                    <div class="pricing-header">
{{--                        <h3 class="pricing-title">Middle Case</h3>--}}
                    </div>
                    <div class="pricing-body">
                        <div class="price-wrapper" style="background: #dad043">
                            <span class="price">حالات متوسطة </span>
                        </div>
                        <ul class="list">
                            <li class="active">4 مرات غسيل في الأسبوع</li>
                            <li class="active">يجب عليه أخذ تمارين رياضية بشكل منتظمة</li>
                            <li class="active">يجب عليه تناول غذاء صحي</li>
                            <li>شرب الكحول</li>
                            <li>شرب السجائر</li>
                            <li>أكل الموالح</li>
                        </ul>
                    </div>
                    <div class="pricing-footer">
                        <a href="{{route("booking")}}" class="main-button">احجز الأن </a>
                    </div>
                </div>
            </div>
            <!-- ***** Pricing Item End ***** -->

            <!-- ***** Pricing Item Start ***** -->
            <div class="col-lg-4 col-md-6 col-sm-12" data-scroll-reveal="enter bottom move 50px over 0.6s after 0.6s">
                <div class="pricing-item">
                    <div class="pricing-header">
{{--                        <h3 class="pricing-title">High Case <small>(Emergency)</small></h3>--}}
                    </div>
                    <div class="pricing-body">
                        <div class="price-wrapper" style="background: #da2e13">
                            <span class="price">الحالات الخطيرة <small>(الطارئة)</small></span>
                        </div>
                        <ul class="list">
                            <li class="active">7 مرات غسيل في الأسبوع</li>
                            <li class="active">يجب عليه أخذ تمارين رياضية بشكل منتظمة</li>
                            <li class="active">يجب عليه تناول غذاء صحي</li>
                            <li>شرب الكحول</li>
                            <li>شرب السجائر</li>
                            <li>أكل الموالح</li>
                        </ul>
                    </div>
                    <div class="pricing-footer">
                        <a href="{{route("booking")}}" class="main-button">Booking Now</a>
                    </div>
                </div>
            </div>
            <!-- ***** Pricing Item End ***** -->
        </div>
    </div>
</section>
<!-- ***** Pricing Plans End ***** -->

<!-- ***** Counter Parallax Start ***** -->
<section class="counter">
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="count-item decoration-bottom">
                        <strong>110</strong>
                        <span>مرضى</span>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="count-item decoration-top">
                        <strong>12</strong>
                        <span>ممرضين</span>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="count-item decoration-bottom">
                        <strong>8</strong>
                        <span>أطباء</span>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="count-item">
                        <strong>27</strong>
                        <span>أجهزة</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ***** Counter Parallax End ***** -->



<!-- ***** Contact Us Start ***** -->
<section class="section colored" id="contact-us">
    <div class="container">
        <!-- ***** Section Title Start ***** -->
        <div class="row">
            <div class="col-lg-12">
                <div class="center-heading">
                    <h2 class="section-title">تحدث معنا</h2>
                </div>
            </div>
            <div class="offset-lg-3 col-lg-6">
                <div class="center-text">
                    <p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.</p>
                </div>
            </div>
        </div>
        <!-- ***** Section Title End ***** -->

        <div class="row">
            <!-- ***** Contact Text Start ***** -->
            <div class="col-lg-4 col-md-6 col-sm-12">
                <h5 class="margin-bottom-30">نحن نسعد بتواصلك معنا <you></you></h5>
                <div class="contact-text">
                    <p>تجدنا متاحين قي أغلب الوقت</p>
                    <p>نحن نحب ان تدعمنا برأيك عنا و عن خدماتنا.</p>
                    <p> <br>مشرفي موقع غسيل الكلى</p>
                </div>
            </div>
            <!-- ***** Contact Text End ***** -->

            <!-- ***** Contact Form Start ***** -->
            <div class="col-lg-8 col-md-6 col-sm-12">
                <div class="contact-form">
                    <form id="contact" action="{{route('dashboard.contact_us.store')}}" method="post">
                        @csrf
                        @method('post')
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <fieldset>
                                    <input name="name" type="text" class="form-control" id="name" placeholder="اسمك كامل" required="">
                                </fieldset>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <fieldset>
                                    <input name="email" type="email" class="form-control" id="email" placeholder="أدخل بريدك الالكتروني" required="">
                                </fieldset>
                            </div>
                            <div class="col-lg-12">
                                <fieldset>
                                    <textarea name="message" rows="6" class="form-control" id="message" placeholder="رسالتك لنا" required=""></textarea>
                                </fieldset>
                            </div>
                            <div class="col-lg-12">
                                <fieldset>
                                    <button type="submit" id="form-submit" class="main-button">Send Message</button>
                                </fieldset>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- ***** Contact Form End ***** -->
        </div>
    </div>
</section>
<!-- ***** Contact Us End ***** -->

<!-- ***** Footer Start ***** -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <ul class="social">
                    <li><a href="{{setting('facebook_link')}}"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="{{setting('twitter_link')}}"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="{{setting('instagram_link')}}"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="{{setting('whatsUp_link')}}"><i class="fa fa-whatsapp"></i></a></li>
                    <li><a href="{{setting('youtubeChanel_link')}}"><i class="fa fa-youtube"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <p class="copyright">{{setting('CopyRight_link')}}</p>
            </div>
        </div>
    </div>
</footer>
@include('footer')
