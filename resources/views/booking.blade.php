@include('header')
<!-- ***** Welcome Area Start ***** -->
<div class="welcome-area" id="welcome">
    <!-- ***** Header Text Start ***** -->
    <div class="header-text">
        <div class="container">
            <div class="row">
                <div class="offset-xl-2 col-xl-12 offset-lg-2 col-lg-8 col-md-12 col-sm-12">
                    <h1 style="margin-right: 50%">رجاءً <strong> املأ </strong><br> <strong> ^-^ </strong>جميع الحقول المتواجدة في الأسفل </h1>

                    <div class="col-lg-8 col-md-6 col-sm-12" >
                        <div class="contact-form" style="margin-right: 5%">
                            @include('dashboard.partials._errors')
                            <form id="contact" action="{{route('store')}}" method="post">
                                @csrf
                                @method('post')
                                <div class="row">
                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <fieldset>
                                            <input name="name" type="text" class="form-control" id="name" placeholder="اسمك كامل" required="">
                                        </fieldset>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <fieldset>
                                            <input name="email" type="email" class="form-control" id="email" placeholder="أدخل بريدك الالكتروني" required="">
                                        </fieldset>
                                    </div><br>
                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <fieldset>
                                            <input name="phone1" type="text" class="form-control" id="email" placeholder="أدخل رقم هاتفك الأول " required="">
                                        </fieldset>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <fieldset>
                                            <input name="phone2" type="text" class="form-control" id="email" placeholder="أدخل رقم هاتفك الثاني" >
                                        </fieldset>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <fieldset>
                                            <input name="sugar_percentage" type="text" class="form-control" id="email" placeholder="نسبة السكر في الدمن" required="">
                                        </fieldset>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <fieldset>
                                            <input name="blood_pressure" type="text" class="form-control" id="email" placeholder="ضغط الدم الخاص بك" required="">
                                        </fieldset>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <fieldset>
                                            <input name="washing_time_week" type="number" class="form-control" id="email" placeholder="عدد مرات الحجز التي تتوقع انك تحتاجها لغسيل الكلى" required="">
                                        </fieldset>
                                    </div>

{{--                                    <div class="col-lg-12">--}}
{{--                                        <fieldset>--}}
{{--                                            <textarea name="message" rows="6" class="form-control" id="message" placeholder="Your Message" required=""></textarea>--}}
{{--                                        </fieldset>--}}
{{--                                    </div>--}}
                                    <div class="col-lg-12">
                                        <fieldset>
                                            <button type="submit" id="form-submit" class="main-button" style="background: #0B90C4;">أرسل</button>
                                        </fieldset>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
{{--                    <a href="{{route('home')}}" class="main-button-slider">Back to Main Page</a>--}}
                </div>
            </div>
        </div>
    </div>
    <!-- ***** Header Text End ***** -->
</div>
<!-- ***** Welcome Area End ***** -->




<!-- ***** Features Small Start ***** -->
<section class="section home-feature">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">

            </div>
        </div>
    </div>
</section>
<!-- ***** Features Small End ***** -->


@include('footer')
