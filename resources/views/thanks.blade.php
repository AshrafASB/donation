@include('header')

<!-- ***** Welcome Area Start ***** -->
<div class="welcome-area" id="welcome">
    <!-- ***** Header Text Start ***** -->
    <div class="header-text">
        <div class="container">
            <div class="row">
                <div class="offset-xl-3 col-xl-6 offset-lg-2 col-lg-8 col-md-12 col-sm-12">
                    <h1>شكرا <strong> لك </strong><br>لتواصلك معنا <strong> ^-^ </strong></h1>
                    <p>لقد تلقينا رسالتك بكل حب، انتظر تواصلنا معك في القريب العاجل</p>
                    <a href="{{route('home')}}" class="main-button-slider">الرجوع للصفحة الرئيسية</a>
                </div>
            </div>
        </div>
    </div>
    <!-- ***** Header Text End ***** -->
</div>
<!-- ***** Welcome Area End ***** -->




<!-- ***** Features Small Start ***** -->
<section class="section home-feature">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">

            </div>
        </div>
    </div>
</section>
<!-- ***** Features Small End ***** -->

@include('footer')
