<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Needy;
use Illuminate\Http\Request;

class NeedyController extends Controller
{
    public function __construct()
    {
        //Parent Path
        $this->path = "dashboard.needies.";

        //Permissions
        $this->middleware('permission:read_needies')->only(['index']);
        $this->middleware('permission:create_needies')->only(['create','store']);
        $this->middleware('permission:update_needies')->only(['edit','update']);
        $this->middleware('permission:delete_needies')->only(['destroy']);

    }

    public function index()
    {
        $needies = Needy::WhenSearch(request()->search)->paginate(5);
        return view($this->path.'index',compact('needies'));
    }//end of index

    public function create()
    {
        return view($this->path.'create');
    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'userName' => 'required|unique:needies,userName',
        ]);

//        $request->only('name','userName','phone','city','address','family_number','notes');
         $data = $request->all()->except(['id_photo','supply_card_photo']);
        dd($data);

        Needy::create($request->all());
        session()->flash('success',__('site.DataAddSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of store

    public function show($id)
    {
        //
    }//end of show

    public function edit(Needy $category)
    {
        return view($this->path.'create',compact('category'));
    }//end of edit

    public function update(Request $request, Needy $category)
    {
        $request->validate([
            'userName' => 'required|unique:needies,userName,'.$category->id,
        ]);
        $category->update($request->all());
        session()->flash('success',__('site.DataUpdatedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of update

    public function destroy(Needy $category)
    {
        $category->delete();
        session()->flash('success',__('site.DataDeletedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of destroy
}
